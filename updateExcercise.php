<?php
	if (isset($_POST["id"])
		&& isset($_POST["discipline_id"]) 
		&& isset($_POST["class_id"]) 
		&& isset($_POST["excercise_type"]) 
		&& isset($_POST["day_of_week"]) 
		&& isset($_POST["time_of_start"]) 
		&& isset($_POST["time_of_end"]) 
		&& isset($_POST["numerator"]))
	{
		$connection = mysqli_connect("localhost", "root", "", "schedule");

	    if (!$connection)
	    {
	        die("Ошибка подключения: " . mysqli_connect_error());
	    }

		// Экранирование
		$id = mysqli_real_escape_string($connection, $_POST["id"]);
		$discipline = mysqli_real_escape_string($connection, $_POST["discipline_id"]);
		$class = mysqli_real_escape_string($connection, $_POST["class_id"]);
		$type = (int)(mysqli_real_escape_string($connection, $_POST["excercise_type"]));
		$day = mysqli_real_escape_string($connection, $_POST["day_of_week"]);
		$start = mysqli_real_escape_string($connection, $_POST["time_of_start"]);
		$end = mysqli_real_escape_string($connection, $_POST["time_of_end"]);
		$numerator = (int)(mysqli_real_escape_string($connection, $_POST["numerator"]));

		$sqlQuery = "UPDATE schedule
			SET discipline_id = $discipline, class_id = $class, excercise_type = $type, day_of_week = $day, time_of_start = '$start', time_of_end = '$end', numerator = $numerator
			WHERE id = $id
		";

		if(mysqli_query($connection, $sqlQuery))
		{
    		header("Location: forAdmin.php");
		}
		else
		{
    		die($connection->error);
		}

		$connection->close();
	}
	else
	{
		die("Заполните недостающую информацию!");
	}
?>