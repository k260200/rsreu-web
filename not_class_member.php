<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    
    <title>Расписание</title>
    
    <link href="./css/bootstrap.min.css" rel="stylesheet">
    <link rel="icon" href="picture/znac.png"/>
   
    <style>
      body { 
       /* Градиент */ 
       background: linear-gradient(#191970, #87CEFA) fixed;
      }
     </style>

  </head>
  <body>
    <nav class="navbar navbar-expand-xl navbar-dark bg-dark">
        <div class="container">
            <a class="navbar-brand" href="main.php"><img src = "picture/icon_home.ico" width="50" height="50"></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbars01" aria-controls="navbars01" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
        <div class="collapse navbar-collapse" id="navbars01">
          <ul class="navbar-nav ">
                <li class="nav-item">
                    <a class="nav-link" href="schedule.php">Расписание</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="groups.php">Группы</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle mr-md-2" href="" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Вход/Выход</a>
                    <div class="dropdown-menu" aria-labelledby="dropdown01">
                      <a class="dropdown-item" href="authorization.php">Авторизация</a>
                      <a class="dropdown-item" href="registration.php">Регистрация</a>
                      <a class="dropdown-item" href="exit.php">Выход</a>
                    </div>
                </li> 
          </ul>
        </div>
      </nav>
    <content class="d-flex justify-content-center m-5">
        <h2>Вы не состоите в группе! Действие невозможно!</h2>
    </content>
    <footer class="text-center text-lg-start bg-dark text-muted fixed-bottom">
        <section class="d-flex justify-content-center justify-content-lg-between border-bottom">
            <div>
                <ul >
                    <li>
                        <a href="http://www.rsreu.ru/" class="link-success">Перейти на сайт РГРТУ</a>
                    </li>
                    <li>
                        <a href="https://cdo.rsreu.ru/" class="link-success">Перейти на сайт СДО</a>
                    </li>
                </ul>
            </div>
        </section>
    </footer>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </body>
</html>