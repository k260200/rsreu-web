<?php

if (!isset($_COOKIE['user']) || !isset($_COOKIE['role']) || $_COOKIE['role'] != 2) {
    header("Location: denied.php");
}

if (isset($_GET["id"]))
	{
		if (!is_int((int)$_GET["id"]))
		{
			die("Некорректный ID!");
		}

		$connection = mysqli_connect("localhost", "root", "", "schedule");

	    if (!$connection)
	    {
	        die("Ошибка подключения: " . mysqli_connect_error());
	    }

		// Экранирование
		$id = (int)(mysqli_real_escape_string($connection, $_GET["id"]));

		$sqlQuery = "SELECT * FROM schedule WHERE `id` = '$id'";
        if ($resultSet = mysqli_query($connection, $sqlQuery)) {
            $excercise = mysqli_fetch_array($resultSet);
        } else {
            echo "Ошибка: " . mysqli_error($connection);
        }

		$connection->close();
	}
	else
	{
		die("ID не задан!");
	}

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">

    <title>add excercise</title>

    <link href="./css/bootstrap.min.css" rel="stylesheet">
    <link rel="icon" href="picture/znac.png" />

    <style>
        body {
            /* Градиент */
            background: whitesmoke fixed;
        }

        html,
        body {
            width: 100%;
            height: 100%;
            margin: 0
        }

        form {
            position: absolute;
            width: 70%;
            height: 50%;
            left: 45%;
            top: 20%;
            margin-left: -100px;
            margin-top: -100px;
            padding: 14px
        }
    </style>
</head>

<body>
    <!-- Форма добавления занятия -->
    <main class="text-center">
        <form class="text-center" method="POST" action="updateExcercise.php">
            <fieldset style="width: 20%">
                <legend>Добавить занятие</legend>
                <input type="hidden" name="id" value="<?php echo $id?>"/>
                <div class="form-group">
                    <p>Предмет: <input type="number" name="discipline_id" class="form-control" value="<?php echo $excercise["discipline_id"] ?>"/></p>
                </div>
                <div class="form-group">
                    <p>Группа: <input type="number" name="class_id" class="form-control" value="<?php echo $excercise["class_id"] ?>"/></p>
                </div>
                <div class="form-group">
                    <p>Тип занятия: <input type="number" name="excercise_type" class="form-control" value="<?php echo $excercise["excercise_type"] ?>"/></p>
                </div>
                <div class="form-group">
                    <p>День недели: <input type="number" name="day_of_week" min="0" max="3000" class="form-control" value="<?php echo $excercise["day_of_week"] ?>"/>
                    </p>
                </div>
                <div class="form-group">
                    <p>Время начала: <input type="time" name="time_of_start" min="0" max="3000" class="form-control" value="<?php echo $excercise["time_of_start"] ?>"/>
                    </p>
                </div>
                <div class="form-group">
                    <p>Время окончания: <input type="time" name="time_of_end" min="0" max="3000" class="form-control" value="<?php echo $excercise["time_of_end"] ?>"/>
                    </p>
                </div>
                <div class="form-group">
                    <p>Числитель: <input type="number" name="numerator" min="0" max="3000" class="form-control" value="<?php echo $excercise["numerator"] ?>"/></p>
                </div>
                <input type="submit" name="submit" class="btn btn-primary" />
            </fieldset>
        </form>
    </main>

</body>
</html>