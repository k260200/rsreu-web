<?php
	if (isset($_GET["id"]))
	{
		if (!is_int((int)$_GET["id"]))
		{
			die("Некорректный ID!");
		}

		$connection = mysqli_connect("localhost", "root", "", "schedule");

	    if (!$connection)
	    {
	        die("Ошибка подключения: " . mysqli_connect_error());
	    }

		// Экранирование
		$id = (int)(mysqli_real_escape_string($connection, $_GET["id"]));

		$sqlQuery = "DELETE FROM schedule WHERE `id` = '$id'";

		if(mysqli_query($connection, $sqlQuery))
		{
    		header("Location: forAdmin.php");
		}
		else
		{
    		die($connection->error);
		}

		$connection->close();
	}
	else
	{
		die("ID не задан!");
	}
?>