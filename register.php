<?php
	
		$connection = mysqli_connect("localhost", "root", "", "schedule");

	    if (!$connection)
	    {
	        die("Ошибка подключения: " . mysqli_connect_error());
	    }

        // Экранирование
        $name = mysqli_real_escape_string($connection, $_POST["name"]);
        $surname = mysqli_real_escape_string($connection, $_POST["surname"]);
        $mail = (mysqli_real_escape_string($connection, $_POST["mail"]));
        $phone = mysqli_real_escape_string($connection, $_POST["phone"]);
		$class = mysqli_real_escape_string($connection, $_POST["class"]);
		$password = mysqli_real_escape_string($connection, $_POST["password"]);
        $login = mysqli_real_escape_string($connection, $_POST["login"]);
        $password = mysqli_real_escape_string($connection, $_POST["password"]);

        // Проверяем корректность логина и пароля
	        if(mb_strlen($login) < 1 || mb_strlen($login) > 30)
	        {
		        echo "Недопустимая длина логина";
		        exit();
	        }
            else if (mb_strlen($password) < 1)
            {
                echo "Пароль слишком короткий";
		        exit();
            }
            // Хешируем пароль
	        // $password = md5($password);
            
            //Проверка на наличие логина в базе
            $resultSet = $connection->query("SELECT * FROM `user` WHERE `login` = '$login'");
            $userCount = mysqli_num_rows($resultSet);
            
	        // Если в базе есть юзер с таким логином, то ошибка
	        if(!empty($userCount))
	        {
		        echo "Пользователь с таким логином уже существует!";
		        exit();
	        }
            else
            {
                $sqlQuery = "INSERT INTO user (`name`, `surname`, `mail`, `phone_number`, `class_id`, `login`, `password`, `role`)
                                VALUES ('$name', '$surname', '$mail', '$phone', '$class', '$login', '$password', '1')";

                if(mysqli_query($connection, $sqlQuery))
                {
					 $resultSet = $connection->query("SELECT * FROM `user` WHERE `login` = '$login' AND `password` = '$password'");
					 $user = $resultSet->fetch_assoc();
					 setcookie('user', $user['login'], time() + 3600, "/");
					 setcookie('role', $user['role'], time() + 3600, "/");
					 setcookie('class', $user['class_id'], time() + 3600, "/");
					
                     header("Location: schedule.php");
                }
                else
                {
                    die($connection->error);
                }

                $connection->close();
            }
?>