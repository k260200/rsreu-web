<?php

if (!isset($_COOKIE['user']) || !isset($_COOKIE['role']) || $_COOKIE['role'] != 2) {
    header("Location: denied.php");
}

$connection = mysqli_connect("localhost", "root", "", "schedule");

if (!$connection) {
    die("Ошибка подключения: " . mysqli_connect_error());
}
$sqlQuery = "SELECT * FROM schedule_view";

if ($resultSet = mysqli_query($connection, $sqlQuery)) {
    $count = mysqli_num_rows($resultSet);
} else {
    echo "Ошибка: " . mysqli_error($connection);
}

mysqli_close($connection);
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">

    <title>Обитель зла</title>

    <link href="./css/bootstrap.min.css" rel="stylesheet">
    <link rel="icon" href="picture/znac.png" />

    <style>
        body {
            /* Градиент */
            background: whitesmoke fixed;
        }
    </style>

</head>

<body>
    <nav class="navbar navbar-expand-xl navbar-dark bg-dark">
        <div class="container">
            <a class="navbar-brand" href="main.php"><img src="picture/icon_home.ico" width="50" height="50"></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbars01"
                aria-controls="navbars01" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbars01">
                <ul class="navbar-nav ">
                    <li class="nav-item dropdown float-right">
                        <a class="nav-link dropdown-toggle mr-md-2" href="" id="dropdown01" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="false">Вход/Выход</a>
                        <div class="dropdown-menu " aria-labelledby="dropdown01">
                            <a class="dropdown-item" href="authorization.php">Авторизация</a>
                            <a class="dropdown-item" href="registration.php">Регистрация</a>
                            <a class="dropdown-item" href="exit.php">Выход</a>
                        </div>
                    </li>
                </ul>
            </div>
    </nav>

    <main class="text-center">
        <div class="container px-4 py-5" id="featured-3">
            <h2 class="pb-2 border-bottom">Корпорация зла! Муа-ха-ха-ха-ха!</h2>
            <div class="table-container">
                <div class="row">
                    <div class="col-md-12">
                        <a class="btn btn-primary" href=\addExcercise.php>Добавить</a>
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Предмет</th>
                                    <th>Группа</th>
                                    <th>Тип занятия</th>
                                    <th>День недели</th>
                                    <th>Начало</th>
                                    <th>Конец</th>
                                    <th>Числитель</th>
                                    <th class="last">Действия</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($resultSet as $excercise): ?>
                                    <tr>
                                        <td>
                                            <?php echo $excercise["discipline"]; ?>
                                        </td>
                                        <td>
                                            <?php echo $excercise["class"]; ?>
                                        </td>
                                        <td>
                                            <?php echo $excercise["excercise type"]; ?>
                                        </td>
                                        <td>
                                            <?php echo $excercise["day of week"]; ?>
                                        </td>
                                        <td>
                                            <?php echo $excercise["start time"]; ?>
                                        </td>
                                        <td>
                                            <?php echo $excercise["end time"]; ?>
                                        </td>
                                        <td>
                                            <?php echo $excercise["numerator"] == 1 ? "numerator" : "denumerator"; ?>
                                        </td>
                                        <td>
                                            <a href=\deleteExcercise.php?id=<?php echo $excercise["id"];?>> Удалить</a><br>
                                            <a href=\editExcercise.php?id=<?php echo $excercise["id"];?>> Редактировать</a>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <?php
        mysqli_free_result($resultSet);
        ?>
        <p>Всего занятий за 2 недели -
            <?php echo "$count" ?>
        </p><br>
        </div>
        </div>
    </main>



    <footer class="text-center text-lg-start bg-dark text-muted fixed-bottom">
        <section class="d-flex justify-content-center justify-content-lg-between  border-bottom">
            <div>
                <ul>
                    <li>
                        <a href="http://www.rsreu.ru/" class="link-success">Перейти на сайт РГРТУ</a>
                    </li>
                    <li>
                        <a href="https://cdo.rsreu.ru/" class="link-success">Перейти на сайт СДО</a>
                    </li>
                </ul>
            </div>
        </section>
    </footer>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
</body>

</html>