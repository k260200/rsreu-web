<?php

if (!isset($_COOKIE['class'])) {
    header("Location: not_class_member.php");
}

if (!isset($_COOKIE['user'])) {
    header("Location: denied.php");
}

$connection = mysqli_connect("localhost", "root", "", "schedule");

if (!$connection) {
    die("Ошибка подключения: " . mysqli_connect_error());
}
$class_id = $_COOKIE['class'];
$sqlQuery = "SELECT * FROM schedule_view WHERE class_id = '$class_id'";

if ($resultSet = mysqli_query($connection, $sqlQuery)) {
    $count = mysqli_num_rows($resultSet);
    $class = mysqli_fetch_array($resultSet)["class"];
} else {
    echo "Ошибка: " . mysqli_error($connection);
}

mysqli_close($connection);
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">

    <title>Расписание</title>

    <link href="./css/bootstrap.min.css" rel="stylesheet">
    <link rel="icon" href="picture/znac.png" />

    <style>
        body {
            /* Градиент */
            background: linear-gradient(#191970, #87CEFA) fixed;
        }
    </style>

</head>

<body>
    <nav class="navbar navbar-expand-xl navbar-dark bg-dark">
        <div class="container">
            <a class="navbar-brand" href="main.php"><img src="picture/icon_home.ico" width="50" height="50"></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbars01"
                aria-controls="navbars01" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbars01">
                <ul class="navbar-nav ">
                    <li class="nav-item">
                        <a class="nav-link" href="schedule.php">Расписание</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="groups.php">Группы</a>
                    </li>
                    <li class="nav-item dropdown float-right">
                        <a class="nav-link dropdown-toggle mr-md-2" href="" id="dropdown01" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="false">Вход/Выход</a>
                        <div class="dropdown-menu " aria-labelledby="dropdown01">
                            <a class="dropdown-item" href="authorization.php">Авторизация</a>
                            <a class="dropdown-item" href="registration.php">Регистрация</a>
                            <a class="dropdown-item" href="exit.php">Выход</a>
                        </div>
                    </li>
                </ul>
            </div>
    </nav>

    <main class="text-center">
        <div class="container px-4 py-5" id="featured-3">
            <h2 class="pb-2 border-bottom">Мое расписание</h2>
            <h5>Расписание группы
                <?php echo "$class" ?>:
            </h5><br>
            <div class="table-container">
                <div class="row">
                    <div class="col-md-12">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Предмет</th>
                                    <th>Вид занятия</th>
                                    <th>День недели</th>
                                    <th>Начало</th>
                                    <th>Конец</th>
                                    <th class="last">Числитель</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($resultSet as $excercise): ?>
                                    <tr>
                                        <td>
                                            <?php echo $excercise["discipline"]; ?>
                                        </td>
                                        <td>
                                            <?php echo $excercise["excercise type"]; ?>
                                        </td>
                                        <td>
                                            <?php echo $excercise["day of week"]; ?>
                                        </td>
                                        <td>
                                            <?php echo $excercise["start time"]; ?>
                                        </td>
                                        <td>
                                            <?php echo $excercise["end time"]; ?>
                                        </td>
                                        <td>
                                            <?php echo $excercise["numerator"] == 1 ? "numerator" : "denumerator"; ?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <?php
        mysqli_free_result($resultSet);
        ?>
        <p>Всего занятий за 2 недели -
            <?php echo "$count" ?>
        </p><br>
        </div>
        </div>
    </main>



    <footer class="text-center text-lg-start bg-dark text-muted fixed-bottom">
        <section class="d-flex justify-content-center justify-content-lg-between  border-bottom">
            <div>
                <ul>
                    <li>
                        <a href="http://www.rsreu.ru/" class="link-success">Перейти на сайт РГРТУ</a>
                    </li>
                    <li>
                        <a href="https://cdo.rsreu.ru/" class="link-success">Перейти на сайт СДО</a>
                    </li>
                </ul>
            </div>
        </section>
    </footer>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
</body>

</html>