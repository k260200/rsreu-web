<?php

// Подключаемся к базе
$connection = mysqli_connect("localhost", "root", "", "schedule");

if (!$connection) {
	die("Ошибка подключения: " . mysqli_connect_error());
}

// Экранируем
$login = mysqli_real_escape_string($connection, $_POST["login"]);
$pass = mysqli_real_escape_string($connection, $_POST["password"]);

// Хешируем пароль
$password = $pass; //md5($pass);

$resultSet = $connection->query("SELECT * FROM `user` WHERE `login` = '$login' AND `password` = '$password'");
$user = $resultSet->fetch_assoc();

// Делаем проверки на существование юзера в базе
if (count($user) == 0) {
	echo "Такой пользователь не найден.";
	exit();
} else if (count($user) == 1) {
	echo "Логин или праоль введены неверно";
	exit();
}

// Если все норм, то заводим куки и перенаправляем на страницу профиля
setcookie('user', $user['login'], time() + 3600, "/");
setcookie('role', $user['role'], time() + 3600, "/");
setcookie('class', $user['class_id'], time() + 3600, "/");

$connection->close();

if ($user['role'] == 1) // студент
{
	header('Location: schedule.php');
} else 
{
	header('Location: forAdmin.php');
}
?>